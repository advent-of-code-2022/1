#!/usr/bin/env python3

import os

def answer(input_file):
    top_three = {}
    top_three_min_calories = 0
    current_elf = 1
    current_calories = 0

    with open(input_file, "r") as input:
        data = input.readlines()

    data.append("\n") # Add an empty line to the end to trigger end-of-elf processing for the final elf if necessary

    for line in data:
        if line != "\n":
            current_calories += int(line)
        else:
            if len(top_three.keys()) < 3:
                top_three[current_calories] = current_elf
                top_three_min_calories = min(top_three.keys())
            elif current_calories > top_three_min_calories:
                top_three.pop(top_three_min_calories)
                top_three[current_calories] = current_elf
                top_three_min_calories = min(top_three.keys())
            current_elf += 1
            current_calories = 0

    print("The three elves with the most calories are:")
    for calories in sorted(top_three, reverse=True):
        print(f"Elf {top_three[calories]} has {calories} calories")
    print(f"Answer: the total calories carried by the top three elves is *** {sum(top_three.keys())} ***")


input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
