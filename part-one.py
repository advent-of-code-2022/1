#!/usr/bin/env python3

import os

def answer(input_file):
    max_calories = 0
    max_elf = -1
    current_elf = 1
    current_calories = 0

    with open(input_file, "r") as input:
        data = input.readlines()

    data.append("\n") # Add an empty line to the end to trigger end-of-elf processing for the final elf if necessary

    for line in data:
        if line != "\n":
            current_calories += int(line)
        else:
            if current_calories > max_calories:
                max_elf = current_elf
                max_calories = current_calories
            current_elf += 1
            current_calories = 0

    print(f"Answer: the maximum number of calories being carried is *** {max_calories} ***, which is being carried by elf number {max_elf}")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
